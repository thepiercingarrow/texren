var Discord = require('discord.io');

var config = require('../config.json');

var bot = new Discord.Client({
    token: config.token,
    autorun: true
});

var texer = require('./texer.js')(bot);

bot.on('ready', function() {
    console.log('ID: ' + bot.id);
});

bot.on('message', function(user, uid, cid, m, e) {
    const guildid = bot.channels[cid].guild_id;
    const prefix = config.prefixes[guildid];
    const errormsg = config.errormsgs[guildid];
    if (m.substring(0, prefix.length) == prefix) {
	texer.render(m.substring(prefix.length), (pngbuf) => {
	    bot.uploadFile({
		to: cid,
		file: pngbuf,
		filename: "rendered.png",
	    });
	}, (err) => {
	    bot.sendMessage({
		to: cid,
		message: errormsg + ' `' + err + '`'
	    });
	});
    }
    if (m.substring(0, config.ivyprefix.length) == config.ivyprefix && uid == config.ivyid) {
	texer.render(m.substring(config.ivyprefix.length + 4), (pngbuf) => {
	    bot.uploadFile({
		to: cid,
		file: pngbuf,
		filename: "rendered.png",
		message: m.substring(config.ivyprefix.length, config.ivyprefix.length + 4)
	    });
	}, (err) => {
	    console.log(err);
	    bot.uploadFile({
		to: cid,
		file: "error.png",
		message: m.substring(config.ivyprefix.length, config.ivyprefix.length + 4)
	    });
	});
    }
});

bot.on('disconnect', function(msg, code) {
    console.log('reconnecting...');
    bot.connect();
});
