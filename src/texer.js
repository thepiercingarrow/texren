var request = require('request');

const REQURL = "https://artofproblemsolving.com/m/texer/ajax.php";

module.exports = function(bot) {
    var texer = {};
    texer.render = function(text, callback, errcallback) {
	var b = new Buffer(text);
	var texerid = Math.random().toString().substring(2);
	request.post(
	    REQURL,
	    { form: {
		action: "image",
		token: "texren" + texerid,
		tex: b.toString('base64'),
		rerender: "false"
	    }},
	    (err, res, body) => {
		if (err)
		    console.log(err);
		var response = JSON.parse(res.body).response;
		if (response.urls == undefined) {
		    errcallback('No image found. (Most likely broken latex.)');
		    return;
		}
		else {
		    request.get(
			{
			    url: 'https:' + JSON.parse(res.body).response.urls[0],
			    encoding: null
			},
			(err, res, pngbuf) => {
			    callback(pngbuf);
			}
		    );
		    console.log(texerid);
		}
	    }
	);
    };
    return texer;
};
